<?php
// Ahmad

abstract class Hewan
{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    
    public function atraksi()
    {
        echo "{$this->nama} sedang {$this->keahlian}<br><br>";
    }
}

trait Fight
{
    public $attackPower;
    public $defencePower;

    public function serang($defender)
    {
        echo "{$this->nama} sedang menyerang {$defender->nama}<br><br>";
        $defender->diserang($this);
    }

    public function diserang($attacker)
    {
        echo "{$this->nama} sedang diserang<br><br>";
        $this->darah -= $attacker->attackPower / $this->defencePower;
    }

    public function getInfoHewan()
    {
        echo "Nama: {$this->nama}<br>";
        echo "Darah: {$this->darah}<br>";
        echo "Jumlah Kaki: {$this->jumlahKaki}<br>";
        echo "Keahlian: {$this->keahlian}<br>";
        echo "Attack Power: {$this->attackPower}<br>";
        echo "Defence Power: {$this->defencePower}<br>";
        echo "Jenis Hewan: ".get_class($this)."<br><br>";
    }
}

class Elang extends Hewan
{
    use Fight;
    
    protected static $counter = 0;

    public function __construct() {
        $this->nama = "elang_".self::$counter;
        $this->jumlahKaki = 2;
        $this->keahlian = 'terbang tinggi';
        $this->attackPower = 10;
        $this->defencePower = 5;
        self::$counter++;
    }
}

class Harimau extends Hewan
{
    use Fight;

    protected static $counter = 0;

    public function __construct() {
        $this->nama = "harimau_".self::$counter;
        $this->jumlahKaki = 4;
        $this->keahlian = 'lari cepat';
        $this->attackPower = 7;
        $this->defencePower = 8;
        self::$counter++;
    }
}

?>